var express = require('express');
var router = express.Router();

var fs = require('fs');

/* GET users listing. */
router.post('/', function (req, res, next) {

    var file1 = req.files.file1;
    var file2 = req.files.file2;

    file1.mv('./files/file1', function (err) {
        if (err)
            return res.status(500).send(err);
    });

    file2.mv('./files/file2', function (err) {
        if (err)
            return res.status(500).send(err);
    });


    fs.readFile('./files/file1', 'utf8', function (err, data1) {
        if (err) {
            return console.log(err);
        }

        fs.readFile('./files/file2', 'utf8', function (err, data2) {
            if (err) {
                return console.log(err);
            }

            var arrFile1 = data1.split("\n");
            var arrFile2 = data2.split("\n");

            var arrResult = arrFile1.map(function (item) {

                if (arrFile2.indexOf(item) != -1) {
                    return {type: "", value: item};
                } else {
                    return {type: "-", value: item};
                }
            });

            arrFile2.forEach(function(item){
                if (arrFile1.indexOf(item) == -1) {
                    arrResult.push({type: "+", value: item});
                }
            });

            res.send(JSON.stringify({'result': arrResult}));

        });
    });
});

module.exports = router;
