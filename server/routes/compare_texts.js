var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function (req, res, next) {

    var text1 = req.body.text1;
    var text2 = req.body.text2;

    var arrFile1 = text1.split("\n");
    var arrFile2 = text2.split("\n");

    var arrResult = arrFile1.map(function (item) {

        if (arrFile2.indexOf(item) != -1) {
            return {type: "", value: item};
        } else {
            return {type: "-", value: item};
        }
    });

    arrFile2.forEach(function(item){
        if (arrFile1.indexOf(item) == -1) {
            arrResult.push({type: "+", value: item});
        }
    });

    res.send(JSON.stringify({'result': arrResult}));
});

module.exports = router;
