import {SET_TEXT_2} from '../actions/index';

export default function (state = '', action) {
    switch (action.type) {
        case SET_TEXT_2:
            return action.payload;
        default:
            return state;
    }
}