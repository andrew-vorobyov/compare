import {SELECT_METHOD_TEXTS, SELECT_METHOD_FILES} from '../actions/index';

export default function (state = 'texts', action) {
    switch (action.type) {
        case SELECT_METHOD_TEXTS:
            return 'texts';
        case SELECT_METHOD_FILES:
            return 'files';
        default:
            return state;
    }
}