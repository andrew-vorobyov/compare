import {SET_TEXT_1} from '../actions/index';

export default function (state = '', action) {
    switch (action.type) {
        case SET_TEXT_1:
            return action.payload;
        default:
            return state;
    }
}