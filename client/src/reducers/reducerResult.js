import {SET_RESULT} from '../actions/index';

export default function (state = [], action) {
    switch (action.type) {
        case SET_RESULT:
            return action.payload.data.result;
        default:
            return state;
    }
}