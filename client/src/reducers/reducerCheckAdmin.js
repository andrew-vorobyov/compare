export default function (state = false, action) {
    switch (action.type) {
        case 'CHECK_ADMIN':
            return action.payload.data.result;
        default:
            return state;
    }
}