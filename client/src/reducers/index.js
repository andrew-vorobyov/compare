import {combineReducers} from 'redux';

import reducerSelectMethods from './reducerSelectMethods';
import reducerText1 from './reducerText1';
import reducerText2 from './reducerText2';
import reducerResult from './reducerResult';

import reducerFile1 from './reducerFile1';
import reducerFile2 from './reducerFile2';

import reducerResultFiles from './reducerResultFiles';

const rootReducer = combineReducers({
    methodInput: reducerSelectMethods,
    text1: reducerText1,
    text2: reducerText2,
    result: reducerResult,
    file1: reducerFile1,
    file2: reducerFile2,
    resultFiles: reducerResultFiles,
})

export default rootReducer;