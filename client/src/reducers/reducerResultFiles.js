import {SET_RESULT_FILES} from '../actions/index';

export default function (state = [], action) {
    switch (action.type) {
        case SET_RESULT_FILES:
            return action.payload.data.result;
        default:
            return state;
    }
}