import React, {Component} from 'react';

import {Provider} from 'react-redux';
import store from './store';

// import './App.css';
import CompareFilesAndTexts from './components/CompareFilesAndTexts';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Provider store={store}>
                    <CompareFilesAndTexts></CompareFilesAndTexts>
                </Provider>
            </div>
        );
    }
}

export default App;
