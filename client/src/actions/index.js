// import {getCheckAdmin} from '../api/apiRequest';

import {compareTextsUrl, compareFilesUrl} from '../config';
import axios from 'axios';

export const SELECT_METHOD_TEXTS = 'SELECT_METHOD_TEXTS';
export const SELECT_METHOD_FILES = 'SELECT_METHOD_FILES';

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

export function selectMethodTexts() {
    return {
        type: SELECT_METHOD_TEXTS,
    }
}

export function selectMethodFiles() {
    return {
        type: SELECT_METHOD_FILES,
    }
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


export const SET_TEXT_1 = 'SET_TEXT_1';
export const SET_TEXT_2 = 'SET_TEXT_2';

export function setText1(payload) {
    return {
        type: SET_TEXT_1,
        payload: payload,
    }
}

export function setText2(payload) {
    return {
        type: SET_TEXT_2,
        payload: payload,
    }
}


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

export const SET_RESULT = 'SET_RESULT';

export function setResult(text1, text2) {

    const request = axios.post(compareTextsUrl, {text1: text1, text2: text2});

    return {
        type: SET_RESULT,
        payload: request,
    }
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

export const SET_FILE_1 = 'SET_FILE_1';
export const SET_FILE_2 = 'SET_FILE_2';

export function setFile1(payload) {
    return {
        type: SET_FILE_1,
        payload: payload,
    }
}

export function setFile2(payload) {
    return {
        type: SET_FILE_2,
        payload: payload,
    }
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

export const SET_RESULT_FILES = 'SET_RESULT_FILES';

export function setResultFiles(file1, file2) {
    const formData = new FormData();
    formData.append('file1', file1);
    formData.append('file2', file2);

    const request = axios.post(compareFilesUrl, formData);

    return {
        type: SET_RESULT_FILES,
        payload: request,
    }
}
