import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {setText1, setText2} from '../actions/index'

class TextsInput extends Component {

    constructor(props) {
        super(props);

        this.handleTextArea1 = this.handleTextArea1.bind(this);
        this.handleTextArea2 = this.handleTextArea2.bind(this);
    }

    handleTextArea1(event) {
        this.props.setText1(event.target.value);
    }

    handleTextArea2(event) {
        this.props.setText2(event.target.value);
    }

    render() {
        return (
            <div className="TextsInput">

                <p>Input text you want to compare below.</p>

                <textarea value={this.props.text1} onChange={this.handleTextArea1}/>
                <textarea value={this.props.text2} onChange={this.handleTextArea2}/>
            </div>
        )
    }
}

export default TextsInput = connect(
    (state) => {
        return {text1: state.text1, text2: state.text2}
    },
    (dispatch) => {
        return bindActionCreators({setText1: setText1, setText2: setText2}, dispatch);
    }
)(TextsInput);