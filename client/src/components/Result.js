import React, {Component} from 'react';

import {connect} from 'react-redux';

class Result extends Component {

    constructor(props) {
        super(props);

        this.renderResult = this.renderResult.bind(this);
        this.renderTable = this.renderTable.bind(this);
    }

    renderTable() {

        let result;

        if (this.props.methodInput === 'texts') {
            result = this.props.result;
        } else if (this.props.methodInput === 'files') {
            result = this.props.resultFiles;
        }

        if (result.length !== 0) {
            return (
                <div>
                    <p>Result of compare</p>

                    <table className="table table-striped table-hover ">
                        <thead>

                        <tr>
                            <th>#</th>
                            <th className="question-title-width">Mark</th>
                            <th className="question-group-width">Value</th>
                        </tr>
                        </thead>

                        <tbody className="container">

                        {this.renderResult()}

                        </tbody>
                    </table>
                </div>
            )
        }
    }

    renderResult() {

        let result;

        if (this.props.methodInput === 'texts') {
            result = this.props.result;
        } else if (this.props.methodInput === 'files') {
            result = this.props.resultFiles;
        }

        if (result.length !== 0) {
            let counter = 0;

            return result.map(item =>
                <tr key={counter}>
                    <td>
                        <span>{++counter} </span><br/>
                    </td>
                    <td>
                        <span>{item.type} </span><br/>
                    </td>
                    <td>
                        <span>{item.value} </span><br/>
                    </td>
                </tr>
            )
        }
    }


    render() {

        return (
            <div className="Result">

                {this.renderTable()}

            </div>
        )
    }
}

export default Result = connect(
    (state) => {
        return {result: state.result, resultFiles: state.resultFiles, methodInput: state.methodInput}
    },
)(Result);