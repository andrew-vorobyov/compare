import React, {Component} from 'react';

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';

import {setFile1, setFile2} from '../actions/index'

class FilesUpload extends Component {

    constructor(props) {

        super(props);

        this.handleInputFile1 = this.handleInputFile1.bind(this);
        this.handleInputFile2 = this.handleInputFile2.bind(this);
    }

    handleInputFile1() {
        this.props.setFile1(this.refs.file1.files[0]);
    }

    handleInputFile2() {
        this.props.setFile2(this.refs.file2.files[0]);
    }

    render() {

        return (
            <div className="FilesUpload">

                <p>Select the files you want to compare below.</p>

                <div>
                    <input type="file" ref="file1"
                           onChange={this.handleInputFile1}/>
                </div>
                <div>
                    <input type="file" ref="file2"
                           onChange={this.handleInputFile2}/>
                </div>

            </div>
        )
    }

}

export default FilesUpload = connect(
    null,
    (dispatch) => {return bindActionCreators({setFile1: setFile1, setFile2: setFile2}, dispatch)}
)(FilesUpload);