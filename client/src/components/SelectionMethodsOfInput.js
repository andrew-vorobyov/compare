import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {selectMethodTexts, selectMethodFiles} from '../actions/index';


class SelectionMethodsOfInput extends Component {

    constructor(props){
        super(props);

        this.handleTextInput = this.handleTextInput.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
    }

    handleTextInput(){
        this.props.selectMethodTexts();
    }

    handleFileUpload(){
        this.props.selectMethodFiles();
    }

    render(){
        return(
            <div className="SelectionMethodsOfInput">

                <button onClick={this.handleTextInput}>Text Input</button> <button onClick={this.handleFileUpload}>File Upload</button>

            </div>
        )
    }

}

export default SelectionMethodsOfInput = connect(
    (state) => {
        return {methodInput: state.methodInput}
    },
    (dispatch) => {
        return bindActionCreators({selectMethodTexts: selectMethodTexts, selectMethodFiles: selectMethodFiles}, dispatch)
    }
)(SelectionMethodsOfInput);