import React, {Component} from 'react';

import Compare from './Compare';
import SelectionMethodsOfInput from './SelectionMethodsOfInput';
import TextsInput from './TextsInput';
import FilesUpload from './FilesUpload';
import Result from './Result';

import {connect} from 'react-redux'

class CompareFilesAndTexts extends Component {

    constructor(props){
        super(props);

        this.renderComponentsInput = this.renderComponentsInput.bind(this);
    }

    renderComponentsInput(){
        if(this.props.methodInput === 'texts'){
            return <TextsInput/>
        } else {
            return <FilesUpload/>
        }
    }


    render() {
        return (
            <div className="CompareFilesAndTexts">

                <h1>Compare 2 texts or files</h1>

                <SelectionMethodsOfInput/>

                {this.renderComponentsInput()}

                <Compare/>

                <Result/>

            </div>
        )
    }

}

export default CompareFilesAndTexts = connect(
    (state) => {
        return {methodInput: state.methodInput}
    }
)(CompareFilesAndTexts);