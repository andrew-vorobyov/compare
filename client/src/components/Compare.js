import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {setResult, setResultFiles} from '../actions/index'


class Compare extends Component {

    constructor(props) {
        super(props);

        this.handleCompare = this.handleCompare.bind(this);
    }

    handleCompare() {

        if (this.props.methodInput === 'texts') {

            if(this.props.text1 === '' || this.props.text2 === ''){
                alert('!!! Your text area is empty, please fill it !!!');
            } else {

                this.props.setResult(this.props.text1, this.props.text2);

            }

        } else if (this.props.methodInput === 'files') {

            if(this.props.file1.name === undefined || this.props.file2.name === undefined){
                alert('!!! Your file area is empty, please choice it !!!');
            } else {

                this.props.setResultFiles(this.props.file1, this.props.file2);

            }
        }
    }

    render() {
        return (
            <div className="Compare">

                <button onClick={this.handleCompare}>Compare</button>

            </div>
        )
    }
}

export default Compare = connect(
    (state) => {
        return {text1: state.text1, text2: state.text2, methodInput: state.methodInput, file1: state.file1, file2: state.file2}
    },
    (dispatch) => {
        return bindActionCreators({setResult: setResult, setResultFiles: setResultFiles}, dispatch)
    }
)(Compare);