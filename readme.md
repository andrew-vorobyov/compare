# Table of Contents
- [Deploy with Docker](#deploy-with-docker)
- [Simple Deploy](#dimple-deploy)

#Deploy with Docker

1 . Go to folder with project and use shell run the command: 
```
docker-compose up -d --build

```
3 . In you browser open http://localhost/

4 . For stop docker use the command Ctrl+C or: 
```
docker-compose stop
```

#Simple Deploy

1 . Go to folder 'client' and run the command: 
```
npm install
npm start
```
3 . Go to folder 'server' and run the command: 
```
npm install
npm start
```